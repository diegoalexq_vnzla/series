import React,{Component} from 'react';
import './Series.scss';
import Search from '../Search';
import Card from'../Generics/Card/';
import api from '../api';
import axios from 'axios';
import { apiKey, language } from '../constants';
import _,{forEach} from 'lodash';

class Series extends Component {
    state ={
        mainData:[]
    }
    
    componentDidMount() {
        axios.all([
            api.get(`tv/top_rated?page=1&language=${language}&api_key=${apiKey}`),
            api.get(`genre/movie/list?api_key=${apiKey}&language=${language}`)
          ])
          .then(axios.spread((series, genres) => {
              _.forEach(series.data.results, (item, key)=> {
                _.forEach(genres.data.genres, (_item, _key)=> {
                    if(_item.id == item.genre_ids[0]){
                        const genres_name = {genres_name:_item.name};
                        Object.assign(series.data.results[key], genres_name);
                    }
                  });
              });
            const mainData = series.data.results;
            this.setState({ mainData });
          }))
          .catch(error => console.log(error));
    }
    render(){
        return (
            <div className="Series container-fluid col-sm-12">
                <Search/>
                <div className="container-subtitle">TODAS</div>
                <div className="container-card">
                {
                    this.state.mainData.map((item) => {
                        return <Card {...item} key={item.id} />
                    })
                }
                </div>
            </div>
        );
    }
}
  
export default Series;
