window.addEventListener('scroll', () => {
  const container = document.getElementsByClassName("detail-img")
  if(container.length > 0){
      scrollFunction();
  }
});

function scrollFunction() {
if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
  document.getElementsByClassName("detail-img")[0].style.height = '85px';
  document.getElementsByClassName("detail-img")[0].style.width = '60px';
} else {
  document.getElementsByClassName("detail-img")[0].style.height = '273px';
  document.getElementsByClassName("detail-img")[0].style.width = '182px';
}
}