import React,{Component} from 'react';
import {withRouter} from 'react-router-dom';
import './ButtonBack.scss';

class ButtonBack extends Component {

    handlerClick = () => {
        this.props.history.push('/')
    };
    render () {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                width="36"
                height="36"
                viewBox="0 0 36 36"
                onClick={this.handlerClick}
                aria-label="[title]"
            >
                <title>Volver</title>
                <defs>
                <path
                    id="b"
                    d="M13.813 27.673c-7.606 0-13.766-6.16-13.766-13.766C.047 6.3 6.207.14 13.813.14 21.42.14 27.58 6.3 27.58 13.907c0 7.606-6.207 13.766-13.767 13.766z"
                />
                <filter
                    id="a"
                    width="150.8%"
                    height="150.8%"
                    x="-25.4%"
                    y="-18.2%"
                    filterUnits="objectBoundingBox"
                >
                    <feOffset dy="2" in="SourceAlpha" result="shadowOffsetOuter1" />
                    <feGaussianBlur
                    in="shadowOffsetOuter1"
                    result="shadowBlurOuter1"
                    stdDeviation="2"
                    />
                    <feColorMatrix
                    in="shadowBlurOuter1"
                    values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08203125 0"
                    />
                </filter>
                </defs>
                <g fill="none" fillRule="evenodd">
                <g fill="#000" opacity="0.9" transform="translate(4 2)">
                    <use filter="url(#a)" xlinkHref="#b" />
                    <use xlinkHref="#b" />
                </g>
                <path
                    fill="#FFF"
                    d="M24.3 15.02H13.847l3.453-3.453a.79.79 0 000-1.074.79.79 0 00-1.073 0l-4.434 4.434a1.183 1.183 0 000 1.68l4.434 4.433c.14.14.326.233.56.233.233 0 .373-.093.56-.233a.79.79 0 000-1.073l-3.454-3.454H24.3c.42 0 .747-.326.747-.746a.737.737 0 00-.747-.747z"
                />
                </g>
            </svg>
        );
    }
}

export default withRouter(ButtonBack);
