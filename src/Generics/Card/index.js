import React,{Component} from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import { urlImg } from '../../constants';
import './Card.scss';


class Card extends Component {
  
    handlerClick = () => {
        this.props.history.push('/detail',{
            name:this.props.name,
            backdrop_path:this.props.backdrop_path,
            poster_path: this.props.poster_path,
            first_air_date: this.props.first_air_date,
            overview: this.props.overview,
        })
    };
    
    render(){
        return (
            <div className="cardContainer col-sm-4" onClick={this.handlerClick}>
                <div className="card">
                    <div className="card-body">
                        <img className="card-img"   src={`${urlImg}${this.props.backdrop_path}`} alt={"img"}/>
                        <h5 className="card-title">{this.props.name}</h5>
                        <div className="card-genres">
                            {this.props.genres_name}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

Card.propTypes = {
    name: PropTypes.string,
    backdrop_path:PropTypes.string,
    poster_path: PropTypes.string,
    first_air_date: PropTypes.string,
    overview: PropTypes.string,
    genre_ids:PropTypes.array,
    genres_name: PropTypes.string
}

export default withRouter(Card)

