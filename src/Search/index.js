import React, { Component } from 'react'
import './Search.scss';

class Search extends Component {
    render() {
        return (
            <div className="input-group mb-3 search">
                <div className="input-group-prepend">
                    <span className="input-group-text">
                        <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 16 16">
                            <path fill="#FFF" fillRule="evenodd" d="M15.805 15.805a.667.667 0 0 1-.943 0L9.74 10.683A5.968 5.968 0 0 1 6 12C2.691 12 0 9.309 0 6c0-3.308 2.692-6 6-6 3.309 0 6 2.692 6 6a5.966 5.966 0 0 1-1.316 3.74l5.12 5.122c.261.26.261.683 0 .943zM6 1.333a4.672 4.672 0 0 0-4.666 4.668A4.672 4.672 0 0 0 6 10.667 4.672 4.672 0 0 0 10.667 6 4.673 4.673 0 0 0 6 1.333z"/>
                        </svg>
                    </span>
                </div>
                <input type="text" className="form-control" placeholder="TV Show Reminder" id="search" name="search" />
            </div>
        )
    }
}

export default Search;
