import React from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Series from './Series';
import detailSeries from './detailSeries';

const routing = () =>
    <Router>
        <Route exact path="/" component={Series} />
        <Route exact path="/detail" component={detailSeries} />
        {/* <Route component={Page404} /> */}
    </Router>

export default routing;
