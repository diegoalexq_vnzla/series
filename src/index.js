import React from 'react';
import {render} from 'react-dom';
import * as serviceWorker from './serviceWorker';

//routes
import Routing from './routes';


render(<Routing/>, document.getElementById('root'))

serviceWorker.unregister();
