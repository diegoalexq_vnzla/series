import React,{Component} from 'react';
import {withRouter} from 'react-router-dom';
import Palette from "react-palette";
import ButtonBack from '../Generics/ButtonBack/';
import { urlImg } from '../constants';
import './detailSeries.scss';
import '../Animation';


class detailSeries extends Component {
    
    render(){
        const detailBack = {
            // backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            opacity: '0.4',
            backgroundImage: `url(${urlImg}${this.props.location.state.backdrop_path})`,
            height:'100vh',
            width: '100vw',
            filter: 'grayscale(1)',
            position: 'absolute',
            top: '0',
            left:'0'

        };
        const detectionColorVibrant =`${urlImg}${this.props.location.state.poster_path}`
  
        return (
            <div className="detailSeries container-fluid col-sm-12" >
                <div className="detail-background-img" style={detailBack}></div>
                <Palette image={detectionColorVibrant}>
                    {palette => (
                        <div style={{
                            backgroundColor: palette.vibrant,
                            height:'100vh',
                            opacity:'0.7',
                            width: '100vw',
                            position: 'absolute',
                            top: '0',
                            left:'0'
                        }}>
                        </div>
                    )}
                </Palette>
                <ButtonBack/>
                <div className="container-detail">
                    <img className="detail-img"   src={`${urlImg}${this.props.location.state.poster_path}`} alt={"img"}/>
                    <div className="detail-title"> {this.props.location.state.name}</div>           
                    <div className="detail-date"> {this.props.location.state.first_air_date.substr(0, 4)}</div>
                    <button type="button" className="btn btn-outline-warning">SUSCRIBIRME</button>

                    <div className="detail-subtitle">OVERVIEW</div>
                    <div className="overview"> 
                        <span>{this.props.location.state.overview}</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(detailSeries)
